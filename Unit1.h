//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <map>
#include <iterator>
#include <algorithm>

//---------------------------------------------------------------------------
class TForm2 : public TForm
{
__published:	// IDE-managed Components
	TLabel *Label2;
	TLabel *Label6;
	TLabel *Label7;
	TEdit *SurnameAdd;
	TEdit *NameAdd;
	TButton *AddB;
	TLabel *Label1;
	TEdit *HeightAdd;
	TLabel *Label3;
	TLabel *Label4;
	TLabel *Label5;
	TLabel *Label8;
	TButton *RemoveB;
	TButton *PrevB;
	TButton *NextB;
	TLabel *SurnameFind;
	TLabel *NameFind;
	TLabel *HeightFind;
	TLabel *Label12;
	TLabel *Label13;
	TLabel *Label14;
	TLabel *Label15;
	TLabel *SurnameShow;
	TLabel *NameShow;
	TLabel *HeightShow;
	TButton *FindB;
	TLabel *Label16;
	void __fastcall AddBClick(TObject *Sender);
	void __fastcall PrevBClick(TObject *Sender);
	void __fastcall NextBClick(TObject *Sender);
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall RemoveBClick(TObject *Sender);
	void __fastcall FindBClick(TObject *Sender);
	void __fastcall SurnameAddClick(TObject *Sender);
private:	// User declarations
public:		// User declarations
	__fastcall TForm2(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE TForm2 *Form2;
//---------------------------------------------------------------------------
class Human {
private:
	UnicodeString surname;
	UnicodeString name;
	int height;
public:

	Human();

	Human (UnicodeString surname, UnicodeString name, int height);

	UnicodeString getSurname() const;

	UnicodeString getName() const;

	int getHeight() const;
};

std::map<int, Human> humans;
std::map<int, Human>::iterator temp = humans.begin();

int count = 0;

void showTemp();
void reableButtons();

bool lowerHeight(const std::pair<int, Human> &lhs, const std::pair<int, Human> &rhs);

#endif
